app.controller('EnrollController', function ($http, $scope, $rootScope,courses) {
        $scope.courses = courses.data;
        console.log($scope.courses);
        $scope.course = null;
        $scope.studentId
        $scope.assignment = null;
        $scope.exam = null;
        $scope.final = null;
        $scope.grade;
        $scope.name;
        $scope.mark;
        $scope.maxMark;
        $scope.competed = false;
        $scope.sId = null;
        $scope.term;

        $scope.findCourse = function () {
            $http({
                url: api + "universities/" + $rootScope.universityID + "/course/" + $scope.sId,
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .success(function (data) {
                    console.log("s");
                    console.log(data);
                    $scope.course = data;
                    $scope.enroll($scope.course);
                    //$scope.courses.push(data);
                })
                .error(function () {
                    alert("Course Not Found");
                    console.log("e");
                });

        }

       /* $scope.getCourses = function () {
            $http({
                url: api + "enroll/user/" + $rootScope.user.RowKey,
                method: "GET",
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .success(function (data) {
                    console.log("s");
                    console.log(data);
                    $scope.courses = data;
                })
                .error(function () {
                    console.log("e");
                });

        };*/

        $scope.enroll = function (val) {
            $http({
                url: api + 'enroll/register',
                method: "POST",
                data: {
                    "StudentId": $rootScope.user.RowKey,
                    "CourseId": val.RowKey,
                    "UniversityId": $rootScope.universityID
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .success(function () {
                    alert("Registered");
                })
                .error(function () {

                });
        }

        $scope.update = function () {
            if ($scope.exam == null) {
                $scope.exam = jQuery.parseJSON($scope.course.CourseWorkMarks);
            }
            if ($scope.assignment == null) {
                $scope.assignment = jQuery.parseJSON($scope.course.Assignments);
            }
            if ($scope.grade == null) {
                $scope.final = $scope.course.Grade;
                $scope.competed = $scope;
            }
            $http({
                url: api + 'enroll/update',
                method: "POST",
                data: {
                    "StudentId": $rootScope.user.RowKey,
                    "CourseId": $scope.course.RowKey,
                    "Assignments": angular.toJson($scope.assignment),
                    "CourseWorkMarks": angular.toJson($scope.exam),
                    "Completed": $scope.completed,
                    "Grade": $scope.final
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .success(function () {

                })
                .error(function () {

                });
        }

        $scope.addAssignment = function () {
            $scope.course = this.course;
            if ($scope.assignment == null) {
                $scope.assignment = jQuery.parseJSON($scope.course.Assignments);
            }
            $scope.assignment.push({
                Name: $scope.name,
                Max: $scope.maxMark,
                Mark: $scope.mark
            });

        }

        $scope.addCwExam = function () {
            $scope.course = this.course;
            if ($scope.exam == null) {
                $scope.exam = jQuery.parseJSON($scope.course.CourseWorkMarks);
            }
            $scope.exam.push({
                Name: $scope.name,
                Max: $scope.maxMark,
                Mark: $scope.mark
            });

            $scope.name = null;
            $scope.maxMark
            $scope.mark = null;
        }

        $scope.addFinal = function () {
            $scope.exam = $scope.grade;
            $scope.competed = true;
            scope.grade = null;


        }
    });