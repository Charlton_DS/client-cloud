var app = angular.module('CloudApp', ['ngRoute', 'ui.bootstrap']);
var api = "http://mcurestapi.azurewebsites.net/";

app.config(function ($routeProvider, $locationProvider) {
  $routeProvider.
  when('/',{
    templateUrl : 'home.html',
  })
  .when('/signup',{
    templateUrl : 'signUp.html',
    controller : 'SignUpController'
  })
  .when('/login',{
    templateUrl : 'login.html',
    controller : 'LoginController'
  })
    .when('/enroll',{
    templateUrl : 'enroll.html',
    controller : 'EnrollController'
  })
    .when('/administration',{
    templateUrl : 'admin.html',
    controller : 'AdminController'
  })
    .when('/courses',{
    templateUrl : 'course.html',
    controller : 'EnrollController',
    resolve : {
                courses : function(CoursesService) {
                    return CoursesService.getCourses();
                }
    }
  })
    .when('/lecturing',{
    templateUrl : 'lecturer.html',
    controller : 'EnrollController',
    resolve : {
                courses : function(CoursesService) {
                    return CoursesService.getCourses();
                }
    }
  })
    .when('/measurements',{
    templateUrl : 'metrics.html',
    controller : 'MetricsController',
    resolve : { 
            metric : function(MetricsService) {
			         return MetricsService.getService();
		          }
          }
  })
    .when('/add/courses',{
    templateUrl : 'registerCourse.html',
    controller : 'RegisterCourseController'
  })
    .when('/add/user',{
    templateUrl : 'registerStudent.html',
    controller : 'RegisterStudentController'
  })
    .when('/students',{
    templateUrl : 'student.html',
    controller : 'StudentController'
  });
  $routeProvider.otherwise({redirectTo : '/'});
});

app.factory("AuthService", function($rootScope, $location, $http){
  return{
    login: function(username, password){        
      if(username != null && password != null){
            return $http({
                url: api + 'auth/login',
                method: "POST",
                data: {"Id" : username, "Password": password},
                headers: {'Content-Type': 'application/json'}
            });
            
          
        }
    },
    logout: function(user){
        if($rootScope.user == user && $rootScope.user != null){
            $rootScope.user = null;
            $location.path('/login');
        }
    }
  };
});

app.factory("MetricsService", function($http,$rootScope){
    return{
        getService: function() {
			var promise = $http({ method: 'GET', url: api +'client/' + $rootScope.universityID }).success(function(data, status, headers, config) {
				return data;
			});
			return promise;
		}
    };

});


app.factory("CoursesService", function($http,$rootScope){
    return{
        getCourses: function() {
			var promise = $http({ method: 'GET', url: api +'enroll/user/' + $rootScope.user.RowKey }).success(function(data, status, headers, config) {
				return data;
			});
			return promise;
		}
    };

});