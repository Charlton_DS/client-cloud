app.controller('SignUpController', function($scope, $http, $rootScope, $location){
    console.log("sign up controller");
    $scope.Name = null;
    $scope.Abbr = null;
    $scope.DomainName = null;
    $scope.Email = null;
    $scope.Password = null;
    $scope.AutoScaling = false;
    $scope.ServiceableUsers = null;

    
    $scope.signup = function(){    
        console.log($scope.Name);
        console.log($scope.Abbr);
        console.log($scope.DomainName);
        console.log($scope.Email);
        console.log($scope.Password);
        console.log($scope.AutoScaling);
        console.log($scope.ServiceableUsers);
        $http({
                url: api + 'universities/create',
                method: "POST",
                data: {"Name" : $scope.Name,
                       "Abbr" : $scope.Abbr,
                       "DomainName" : $scope.DomainName,
                       "AutoScaling" : $scope.AutoScaling,
                       "ServiceableUsers" : $scope.ServiceableUsers},
                headers: {'Content-Type': 'application/json'}
            })
        .success(function(data){
            console.log("s"); 
            console.log(data); 
            $rootScope.universityID = data;
            addAdmin(data);  
        })
        .error(function(){
            console.log("e"); 
        });  
        
    }
    
    var addAdmin = function(universityID){
     $http({
                url: api + 'universities/student',
                method: "POST",
                data: {"PartitionKey" : universityID,
                        "RowKey": $scope.Abbr,
                        "Role" : "Admin",
                       "Password" : $scope.Password,
                       "Email" : $scope.Email,
                       "FirstName" : $scope.Name,
                       "LastName" : $scope.Abbr
                      },
                headers: {'Content-Type': 'application/json'}
            })
            .success(function(data){
                console.log("s"); 
                console.log(data); 
                $rootScope.user = data; 
                $location.path('/measurements');
            })
            .error(function(){
                console.log("e"); 
            });  
    }

});