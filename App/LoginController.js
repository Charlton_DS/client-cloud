app.controller('LoginController', function ($scope, $rootScope, $http, $location, AuthService) {
            
    $rootScope.user = null;
    $scope.username = null;
    $scope.password = null;
    $scope.failed = false;
    
    
    $scope.login = function login() {
        AuthService.login($scope.username, $scope.password)
        .then(function(response){
                console.log("success");
                $rootScope.user = response.data;  
                console.log(response);
                $scope.failed = false;
                $rootScope.universityID = $rootScope.user.PartitionKey;
                if($rootScope.user.Role == "Admin") $location.path('#/measuring');  
                if($rootScope.user.Role == "Student") $location.path('#/courses'); 
                if($rootScope.user.Role == "Lecturer") $location.path('#/lecturing'); 
                
            },function(response){
                $scope.failed = true;
            });
    }
    
});