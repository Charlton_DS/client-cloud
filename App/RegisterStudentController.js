app.controller('RegisterStudentController', function ($http, $scope, $rootScope){
    
    
    $scope.students = [];
    $scope.sId = null;
    $scope.FirstName = null;
    $scope.MiddleName = null;
    $scope.LastName = null;
    $scope.Status = null;
    $scope.Email = null;
    $scope.Role = null;
    $scope.PhoneNumber = null;
    $scope.Address = null;
    $scope.DOB = null;
    $scope.Password = null;
    
    
    $scope.addStudent = function(){
        if($scope.sId != null && $scope.FirstName != null && $scope.MiddleName != null && $scope.LastName != null &&
           $scope.Status != null && $scope.Email != null && $scope.DOB != null &&
           $scope.PhoneNumber != null && $scope.Address != null){
           
            $scope.students.push({
            PartitionKey : $rootScope.universityID,
            RowKey : $scope.sId,
            Password : $scope.Password,
            FirstName : $scope.FirstName,
            MiddleName : $scope.MiddleName,
            LastName : $scope.LastName,
            Status : $scope.Status,
            Role : $scope.Role,
            Email : $scope.Email,
            PhoneNumber : $scope.PhoneNumber,
            Address : $scope.Address,
            DOB : $scope.DOB});
            
       }
        console.log($scope.students);
        $scope.sId = null;
        $scope.FirstName = null;
        $scope.MiddleName = null;
        $scope.LastName = null;
        $scope.Role = null;
        $scope.Status = null;
        $scope.Email = null;
        $scope.PhoneNumber = null;
        $scope.Address = null;
        $scope.DOB = null;
        $scope.Password = null;
    }
    
    $scope.send = function(){
        $http({
                url: api + 'universities/students',
                method: "POST",
                data: angular.toJson($scope.students),
                headers: {'Content-Type': 'application/json'}
            })
            .success(function(){
                       alert("entered");
            })
            .error(function(){
                
            });  
    
    }

});