app.controller('RegisterCourseController', function ($http, $scope, $rootScope){
    
    $scope.courses = [];
    $scope.sId = null;
    $scope.Name = null;
    $scope.Programme = null;
    $scope.Level = null;
    $scope.MaxStudents = null;
    $scope.NumStudents = null;
    $scope.Semester = null;
    
    $scope.addCourse = function(){
        if($scope.sId != null && $scope.Name != null && $scope.Programme != null &&
           $scope.Level != null && $scope.MaxStudents != null && $scope.Semester != null){
            console.log("ented");
            $scope.courses.push({
            PartitionKey : $rootScope.universityID,
            RowKey : $scope.sId,
            Name : $scope.Name,
            Programme : $scope.Programme,
            Level : $scope.Level,
            MaxStudemt : $scope.MaxStudents,
            NumStudents : 0,
            Semester : $scope.Semester});
        }
        console.log($scope.courses);
        $scope.sId = null;
        $scope.Name = null;
        $scope.Programme = null;
        $scope.Level = null;
        $scope.MaxStudents = null;
        $scope.NumStudents = null;
        $scope.Semester = null;
    }
    
    $scope.send = function(){
        $http({
                url: api + 'universities/courses',
                method: "POST",
                data: angular.toJson($scope.courses),
                headers: {'Content-Type': 'application/json'}
            })
            .success(function(){
                     
            })
            .error(function(){
                
            });  
    
    }

});